# MVVM Template Engine
This is an example of how one can implement template engine with model-view-viewmodel bindings and "Ahead-of-time" compilation in 1 week.

First version, was made to argue, more recent version may be found as a part of "Minimal SPA framework" package

####Running:
* `npm install`
* `npm start`
* see localhost:3000

####Demonstrates:
* Using angular-like template logic with basic arithmetic operations (src/components/test.component.html)
* Using nested components, arrays and loops (src/components/retrieve-api-test/retrieve-api-test.component.html)
* Handling big amounts (N = 3000) of views with data sync effectively (src/components/list/list.component.html)
